/* Copyright 2023 @ Keychron (https://www.keychron.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

// -- MODS -- //

// HOME MODS
#define HOME_A    LSFT_T(KC_A)
#define HOME_S    LCTL_T(KC_S)
#define HOME_D    LGUI_T(KC_D)
#define HOME_F    LALT_T(KC_F)
#define HOME_J    RALT_T(KC_J)
#define HOME_K    RGUI_T(KC_K)
#define HOME_L    RCTL_T(KC_L)
#define HOME_SCLN RSFT_T(KC_SCLN)

// OTHER MODS
#define SPACE     LT(MOVE, KC_SPC)

// -- SHORTENING -- //

// MOUSE WHEEL
#define WH_DOWN   KC_MS_WH_DOWN
#define WH_UP     KC_MS_WH_UP

// MOUSE DIRECTIONS
#define MS_LEFT   KC_MS_LEFT
#define MS_DOWN   KC_MS_DOWN
#define MS_UP     KC_MS_UP
#define MS_RIGHT  KC_MS_RIGHT

// MOUSE BUTTONS
#define MS_1      KC_MS_BTN1
#define MS_2      KC_MS_BTN2
#define MS_3      KC_MS_BTN3
#define MS_4      KC_MS_BTN4


enum layers {
    TYPE,
    GAME,
    MOVE,
    RGBA,
};

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [TYPE] = LAYOUT_109_iso(
        KC_ESC,             KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,    KC_F6,    KC_F7,    KC_F8,    KC_F9,    KC_F10,   KC_F11,   KC_F12,  KC_PSCR,  KC_CTANA, RGB_MOD, TO(TYPE),TO(MOVE),TO(GAME),TO(RGBA),
        KC_GRV,   KC_1,     KC_2,     KC_3,     KC_4,     KC_5,     KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     KC_MINS,  KC_EQL,   KC_BSPC, KC_INS,   KC_HOME,  KC_PGUP, KC_NUM,  KC_PSLS, KC_PAST, KC_PMNS,
        KC_TAB,   KC_Q,     KC_W,     KC_E,     KC_R,     KC_T,     KC_Y,     KC_U,     KC_I,     KC_O,     KC_P,     KC_LBRC,  KC_RBRC,           KC_DEL,   KC_END,   KC_PGDN, KC_P1,   KC_P2,   KC_P3,   KC_PPLS,
        KC_ESC,   HOME_A,   HOME_S,   HOME_D,   HOME_F,   KC_G,     KC_H,     HOME_J,   HOME_K,   HOME_L,   HOME_SCLN,KC_QUOT,  KC_NUHS,  KC_ENT,                               KC_P4,   KC_P5,   KC_P6,
        KC_LSFT,  KC_NUBS,  KC_Z,     KC_X,     KC_C,     KC_V,     KC_B,     KC_N,     KC_M,     KC_COMM,  KC_DOT,   KC_SLSH,            KC_RSFT,           KC_UP,             KC_P7,   KC_P8,   KC_P9,   KC_PENT,
        KC_LCTL,  KC_LWIN,  KC_LALT,                                SPACE,                                  KC_RALT,  KC_RWIN,  _______,  KC_RCTL, KC_LEFT,  KC_DOWN,  KC_RGHT, KC_P0,            KC_PDOT        ),

    [GAME] = LAYOUT_109_iso(
        KC_ESC,             KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,    KC_F6,    KC_F7,    KC_F8,    KC_F9,    KC_F10,   KC_F11,   KC_F12,  KC_PSCR,  KC_CTANA, RGB_MOD, _______, _______, _______, _______,
        KC_GRV,   KC_1,     KC_2,     KC_3,     KC_4,     KC_5,     KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     KC_MINS,  KC_EQL,   KC_BSPC, KC_INS,   KC_HOME,  KC_PGUP, KC_NUM,  KC_PSLS, KC_PAST, KC_PMNS,
        KC_TAB,   KC_Q,     KC_W,     KC_E,     KC_R,     KC_T,     KC_Y,     KC_U,     KC_I,     KC_O,     KC_P,     KC_LBRC,  KC_RBRC,           KC_DEL,   KC_END,   KC_PGDN, KC_P1,   KC_P2,   KC_P3,   KC_PPLS,
        KC_ESC,   KC_A,     KC_S,     KC_D,     KC_F,     KC_G,     KC_H,     KC_J,     KC_K,     KC_L,     KC_SCLN,  KC_QUOT,  KC_NUHS,  KC_ENT,                               KC_P4,   KC_P5,   KC_P6,
        KC_LSFT,  KC_NUBS,  KC_Z,     KC_X,     KC_C,     KC_V,     KC_B,     KC_N,     KC_M,     KC_COMM,  KC_DOT,   KC_SLSH,            KC_RSFT,           KC_UP,             KC_P7,   KC_P8,   KC_P9,   KC_PENT,
        KC_LCTL,  KC_LWIN,  KC_LALT,                                KC_SPC,                                 KC_RALT,  KC_RWIN,  _______,  KC_RCTL, KC_LEFT,  KC_DOWN,  KC_RGHT, KC_P0,            KC_PDOT        ),

    [MOVE] = LAYOUT_109_iso(
        _______,            KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,    KC_F6,    KC_F7,    KC_F8,    KC_F9,    KC_F10,   KC_F11,   KC_F12,  _______,  _______,  RGB_TOG, _______, _______, _______, _______,
        _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______, _______,  _______,  _______, _______, _______, _______, _______,
        _______,  WH_DOWN,  MS_UP,    WH_UP,    _______,  _______,  _______,  _______,   _______,  KC_ENT,  _______,  _______,  _______,           _______,  _______,  _______, _______, _______, _______, _______,
        _______,  MS_LEFT,  MS_DOWN,  MS_RIGHT, KC_BSPC,  KC_DEL,   KC_LEFT,  KC_DOWN,  KC_UP,    KC_RGHT,  MS_1,     MS_2,     MS_3,     MS_4,                                 _______, _______, _______,
        MO(RGBA), _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,            _______,           _______,           _______, _______, _______, _______,
        _______,  _______,  _______,                                _______,                                _______,  _______,  _______,  _______, _______,  _______,  _______, _______,          _______        ),

    [RGBA] = LAYOUT_109_iso(
        KC_CAPS,            KC_BRID,  KC_BRIU,  KC_TASK,  KC_FILE,  RGB_VAD,  RGB_VAI,  KC_MPRV,  KC_MPLY,  KC_MNXT,  KC_MUTE,  KC_VOLD,  KC_VOLU, _______,  _______,  RGB_TOG, _______, _______, _______, _______,
        _______,  BT_HST1,  BT_HST2,  BT_HST3,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______, _______,  _______,  _______, _______, _______, _______, _______,
        RGB_TOG,  RGB_MOD,  RGB_VAI,  RGB_HUI,  RGB_SAI,  RGB_SPI,  _______,  _______,  _______,  _______,  _______,  _______,  _______,           _______,  _______,  _______, _______, _______, _______, _______,
        _______,  RGB_RMOD, RGB_VAD,  RGB_HUD,  RGB_SAD,  RGB_SPD,  _______,  _______,  _______,  _______,  _______,  _______,  _______,  _______,                              _______, _______, _______,
        _______,  _______,  _______,  _______,  _______,  _______,  BAT_LVL,  NK_TOGG,  _______,  _______,  _______,  _______,            _______,           _______,           _______, _______, _______, _______,
        _______,  _______,  _______,                                _______,                                _______,  _______,  _______,  _______, _______,  _______,  _______, _______,          _______        ),
};
